﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.IO;
using System.Net;
using System.Text;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.S3.Encryption;
using Amazon.KeyManagementService;

using Amazon.KeyManagementService.Model;
namespace AWS_GUI
{
    class S3
    {
        static IAmazonS3       Client;
        static TransferUtility TransferUtility;

        static List < File > FilesHashes = new List < File > ( );
        static Mutex         Mutex       = new Mutex ( );

        NameValueCollection appConfig = ConfigurationManager.AppSettings;

        Debug Debug = new Debug ( "AWS", true );
        Uploader Uploader = new Uploader ( );

        public S3 ( )
        {
            if ( CheckRequiredFields ( ) )
            {
                Client = new AmazonS3Client ( );
                TransferUtility = new TransferUtility ( Client );
            }

            Uploader.Show ( );
        }

        public bool UploadingAnObject ( File file )
        {
            try
            {
                String Key  = file.TimeStamp;
                String Hash = file.HashMD5;

                UpdateHashList ( );

                if ( NewObject ( file ) )
                {
                    TransferUtilityUploadRequest request = new TransferUtilityUploadRequest
                    {
                        BucketName = appConfig [ "AWSBucketName" ],
                        Key = Key,
                        FilePath = file.FullPath,
                        ServerSideEncryptionMethod = ServerSideEncryptionMethod.AWSKMS
                    };

                    request.Metadata.Add ( "md5", file.HashMD5 );

                    request.UploadProgressEvent += new EventHandler < UploadProgressArgs > ( Uploader.ProgressEvent );

                    Uploader.AddUpload ( file );

                    Debug.AppendText ( "=======================================" );
                    Debug.AppendText ( "Uploading " + Key + "..." );

                    TransferUtility.Upload ( request );

                    Uploader.RemoveUpload ( file );

                    Debug.AppendText ( "Done" );
                }

                else
                {
                    Debug.AppendText ( Key + " already exists in AWS S3" );

                    //Uploader.RemoveUpload ( file );
                }

                return true;
            }

            catch ( AmazonS3Exception amazonS3Exception )
            {
                AmazonS3_ExceptionHandler ( amazonS3Exception );

                Uploader.RemoveUpload ( file );

                return false;
            }
        }

        // Change the AWSProfileName to the profile you want to use in the App.config file.
        // Change the AWSBucketName to the profile you want to use in the App.config file.
        // See http://aws.amazon.com/credentials  for more details.
        // You must also sign up for an Amazon S3 account for this to work
        // See http://aws.amazon.com/s3/ for details on creating an Amazon S3 account

        private bool CheckRequiredFields ( )
        {
            if ( string.IsNullOrEmpty ( appConfig [ "AWSProfileName" ] ) )
            {
                Debug.AppendText ( "AWSProfileName was not set in the App.config file." );

                return false;
            }

            if ( string.IsNullOrEmpty ( appConfig [ "AWSBucketName" ] ) )
            {
                Debug.AppendText ( "AWSBucketName was not set in the App.config file." );

                return false;
            }

            return true;
        }

        private void UpdateHashList ( )
        {
            // Wait until it is safe to enter.
            Mutex.WaitOne ( );

            try
            {
                ListObjectsV2Request request = new ListObjectsV2Request
                {
                    BucketName = appConfig [ "AWSBucketName" ],
                    MaxKeys = 10
                };

                ListObjectsV2Response response;

                do
                {
                    response = Client.ListObjectsV2 ( request );

                    if ( 0 < response.S3Objects.Count )
                    {
                        FilesHashes.Clear ( );
                    
                        // Process response.
                        foreach ( S3Object entry in response.S3Objects )
                        {
                            File file = new File ( );

                            file.TimeStamp = entry.Key;
                            file.HashMD5   = ReadObjectData ( entry.Key );

                            FilesHashes.Add ( file );
                        }
                    }

                    request.ContinuationToken = response.NextContinuationToken;
                } while ( response.IsTruncated );
            }

            catch ( AmazonS3Exception amazonS3Exception )
            {
                AmazonS3_ExceptionHandler ( amazonS3Exception );
            }

            finally
            {
                // Release the Mutex.
                Mutex.ReleaseMutex ( );
            }
        }

        private String ReadObjectData ( String Key )
        {
            String HashMD5 = String.Empty;

            // Put a more complex object with some metadata and http headers.
            GetObjectRequest request = new GetObjectRequest ( )
            {
                BucketName = appConfig [ "AWSBucketName" ],
                Key = Key
            };

            using ( GetObjectResponse response = Client.GetObject ( request ) )
            using ( Stream responseStream = response.ResponseStream )
            using ( StreamReader reader = new StreamReader ( responseStream ) )
            {
                HashMD5 = response.Metadata [ "md5" ];
            }

            return HashMD5;
        }

        private bool NewObject ( File file )
        {
            bool new_file = true;

            // Wait until it is safe to enter.
            Mutex.WaitOne ( );

            // If the bucket is empty - to prevent multiples updates of the same file at once
            if ( 0 == FilesHashes.Count )
            {
                FilesHashes.Add ( file );
            }

            else
            {
                foreach ( File aws_file in FilesHashes )
                {
                    if ( aws_file.HashMD5 == file.HashMD5 )
                    {
                        new_file = false;

                        break;
                    }
                }
            }

            // Release the Mutex.
            Mutex.ReleaseMutex ( );

            return new_file;
        }

        private void AmazonS3_ExceptionHandler ( AmazonS3Exception AmazonS3Exception )
        {
            if ( ( AmazonS3Exception.ErrorCode != null ) &&
                 ( AmazonS3Exception.ErrorCode.Equals ( "InvalidAccessKeyId" ) ||
                   AmazonS3Exception.ErrorCode.Equals ( "InvalidSecurity" ) ) )
            {
                Debug.AppendText ( "Please check the provided AWS Credentials." );
                Debug.AppendText ( "If you haven't signed up for Amazon S3, please visit http://aws.amazon.com/s3" );
            }

            else
            {
                Debug.AppendText ( String.Format ( "An error occurred with the message '{0}' when deleting an object. ", AmazonS3Exception.Message ) );
            }
        }
    }
}
