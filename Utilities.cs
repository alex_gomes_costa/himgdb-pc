﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWS_GUI
{
    class Utilities
    {
        static List < String > SizeUnits = new List < string >
        {
            "bytes",
            "KB",
            "MB",
            "GB",
            "TB",
            "PB",
            "EB",
            "ZB",
            "YB"
        };

        public static String FormatSize ( double size )
        {
            try
            {
                double mantisse = Math.Truncate ( System.Math.Log ( size, 1024 ) );

                return String.Format ( "{0:0.0} {1}", size / System.Math.Pow ( 1024, mantisse ), SizeUnits [ Convert.ToInt16 ( mantisse ) ] );
            }

            catch
            {
                return String.Empty;
            }
        }

        public static String FormatTime ( double seconds )
        {
            try
            {
                TimeSpan time = TimeSpan.FromSeconds ( seconds );

                // Here backslash is must to tell that colon is
                // not the part of format, it just a character that we want in output
                return time.ToString ( @"hh\:mm\:ss" );
            }

            catch
            {
                return String.Empty;
            }
        }
    }
}
