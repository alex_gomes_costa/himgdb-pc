﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AWS_GUI
{
    class FileExplorer
    {
        // Create an instance of the open file dialog box.
        OpenFileDialog OpenFileDialog = new OpenFileDialog ( );

        public FileExplorer ( ) : this ( String.Empty )
        {
        }

        public FileExplorer ( String InitialDirectory )
        {
            if ( string.IsNullOrEmpty ( InitialDirectory ) )
            {
                InitialDirectory = System.Environment.GetFolderPath ( Environment.SpecialFolder.Desktop );
            }

            OpenFileDialog.FilterIndex = 1;
            OpenFileDialog.Multiselect = false;
            OpenFileDialog.Filter = "All Files (*.*)|*.*|Text Files (.txt)|*.txt|PDF Files (.pdf)|*.pdf";
            OpenFileDialog.InitialDirectory = InitialDirectory;
        }

        public String GetFilePath ( String Title = "Abrir" )
        {
            // Set the window title
            OpenFileDialog.Title = Title;

            // Process input if the user clicked OK.
            if ( DialogResult.OK == OpenFileDialog.ShowDialog ( ) )
            {
                return OpenFileDialog.FileName;
            }

            return "";
        }
    }
}
