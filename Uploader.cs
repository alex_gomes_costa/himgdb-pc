﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;

namespace AWS_GUI
{
    public partial class Uploader : Form
    {
        static List < float >       MinCellWidth = new List < float > ( );
        static List < UploadTable > UploadTables = new List < UploadTable > ( );
        static Mutex                Mutex        = new Mutex ( );

        delegate void AddUploadCallback    ( File file );
        delegate void RemoveUploadCallback ( File file );

        bool  Resizing      = false;
        int   Column        = -1;
        int   StartCursor   = -1;
        float OriginalWidth = 0;

        public Uploader ( )
        {
            InitializeComponent ( );

            MinCellWidth.Add ( 6 + this.Position_Label.Width );
            MinCellWidth.Add ( 6 + this.Name_Label.Width );
            MinCellWidth.Add ( 6 + this.Size_Label.Width );
            MinCellWidth.Add ( 6 + this.Progress_Label.Width );
            MinCellWidth.Add ( 6 + this.Upload_Label.Width );
            MinCellWidth.Add ( 0 + this.RemainingTime_Label.Width );
        }

        private void Uploader_MouseDown ( object sender, MouseEventArgs e )
        {
            if ( MouseButtons.Left == e.Button )
            {
                if ( ( 0 <= Column ) && ( !Resizing ) )
                {
                    StartCursor   = e.X;
                    OriginalWidth = this.UploadInfos_TableLayoutPanel.ColumnStyles [ Column ].Width;
                    Resizing      = true;
                }
            }
        }

        private void Uploader_MouseMove ( object sender, MouseEventArgs e )
        {
            Control c = ( Control ) sender;

            if ( !Resizing )
            {
                float Width_Sum = 0;

                this.Column = -1;

                for ( int Cell = 0; Cell < this.UploadInfos_TableLayoutPanel.ColumnStyles.Count; Cell ++ )
                {
                    Width_Sum += this.UploadInfos_TableLayoutPanel.ColumnStyles [ Cell ].Width + 2;

                    if ( 3 >= System.Math.Abs ( Width_Sum - e.X ) )
                    {
                        this.Column = Cell;

                        break;
                    }
                }

                if ( 0 <= this.Column )
                {
                    this.UploadInfos_TableLayoutPanel.Cursor = Cursors.VSplit;
                }

                else
                {
                    this.UploadInfos_TableLayoutPanel.Cursor = Cursors.Default;
                }
            }

            else
            {
                float New_Width = OriginalWidth + Convert.ToSingle ( e.X - StartCursor );
                float MaxCellWidth = 0;

                MaxCellWidth = this.UploadInfos_TableLayoutPanel.Width - MinCellWidth.Sum ( ) + MinCellWidth [ Column ] - 20;

                if ( ( MinCellWidth [ Column ] <= New_Width ) && ( MaxCellWidth >= New_Width ) )
                {
                    if ( ( TableWidth ( ) - this.UploadInfos_TableLayoutPanel.ColumnStyles [ Column ].Width + New_Width ) <= this.UploadInfos_TableLayoutPanel.Width )
                    {
                        this.UploadInfos_TableLayoutPanel.ColumnStyles [ Column ].Width = New_Width;

                        // Wait until it is safe to enter.
                        Mutex.WaitOne ( );

                        foreach ( UploadTable Table in UploadTables )
                        {
                            Table.ColumnStyles [ Column ].Width = New_Width;
                        }

                        // Release the Mutex.
                        Mutex.ReleaseMutex ( );

                        this.Invalidate ( );
                    }
                }
            }
        }

        private void Uploader_MouseUp ( object sender, MouseEventArgs e )
        {
            this.UploadInfos_TableLayoutPanel.Cursor = Cursors.Default;

            if ( System.Windows.Forms.MouseButtons.Left == e.Button )
            {
                Column      = -1;
                StartCursor = -1;

                Resizing = false;
            }
        }

        private float TableWidth ( )
        {
            float Width = MinCellWidth [ MinCellWidth.Count - 1 ] + 20;

            foreach ( ColumnStyle Column in this.UploadInfos_TableLayoutPanel.ColumnStyles )
            {
                Width += Column.Width;
            }

            return Width;
        }

        public void ProgressEvent ( object sender, UploadProgressArgs e )
        {
            System.DateTime Now = System.DateTime.Now;

            String key = ( ( Amazon.S3.Transfer.TransferUtilityUploadRequest ) sender ).Key;

            // Wait until it is safe to enter.
            Mutex.WaitOne ( );

            foreach ( UploadTable Upload in UploadTables )
            {
                if ( key == Upload.GetName ( ) )
                {
                    double TimeElapsed = ( Convert.ToDouble ( Now.Ticks - Upload.Time.Ticks ) / Convert.ToDouble ( TimeSpan.TicksPerSecond ) );
                    
                    if ( 1 < TimeElapsed )
                    {
                        double UploadRate    = ( ( e.TransferredBytes - Upload.GetTransferredBytes ( ) ) / TimeElapsed );
                        double RemainingTime = ( ( e.TotalBytes - e.TransferredBytes ) / UploadRate );

                        if ( 0 < UploadRate )
                        {
                            Upload.SetUploadRate ( UploadRate );
                            Upload.SetRemainingTime ( RemainingTime );
                        }

                        else
                        {
                            Upload.SetUploadRate ( 0 );
                            Upload.SetRemainingTime ( 0 );
                        }

                        Upload.SetProgressPercentage ( e.PercentDone );
                        Upload.SetTransferredBytes ( e.TransferredBytes );
                        Upload.Time = Now;
                    }

                    break;
                }
            }

            // Release the Mutex.
            Mutex.ReleaseMutex ( );

            this.Invalidate ( );
        }

        public void AddUpload ( File file )
        {
            if ( this.InvokeRequired )
            {
                AddUploadCallback d = new AddUploadCallback ( AddUpload );
                this.Invoke ( d, new object [ ] { file } );
            }

            else
            {
                UploadTable Upload = new UploadTable ( file );

                this.Controls.Add ( Upload.TableLayoutPanel );

                // Wait until it is safe to enter.
                Mutex.WaitOne ( );

                UploadTables.Add ( Upload );

                // Release the Mutex.
                Mutex.ReleaseMutex ( );

                this.Invalidate ( );
            }
        }

        public void RemoveUpload ( File file )
        {
            if ( this.InvokeRequired )
            {
                RemoveUploadCallback d = new RemoveUploadCallback( RemoveUpload );
                this.Invoke ( d, new object [ ] { file } );
            }

            else
            {
                // Wait until it is safe to enter.
                Mutex.WaitOne ( );
                
                foreach ( UploadTable Upload in UploadTables )
                {
                    if ( file.TimeStamp == Upload.GetName ( ) )
                    {
                        this.Controls.Remove ( Upload.TableLayoutPanel );
                        UploadTables.Remove ( Upload );

                        Upload.Dispose ( );

                        break;
                    }
                }

                UploadTables.Sort ( ( a, b ) => a.GetPosition ( ).CompareTo ( b.GetPosition ( ) ) );

                int position = 0;

                foreach ( UploadTable Upload in UploadTables )
                {
                    Upload.RefreshPosition ( ++position );
                }

                // Release the Mutex.
                Mutex.ReleaseMutex ( );

                this.Invalidate ( );
            }
        }
    }
    
    class UploadTable : IDisposable
    {
        private static int TabIndex = 0;
        private static int TabShown = 0;

        private long TransferredBytes = 0;

        private System.Windows.Forms.Label            Position_Label      = new System.Windows.Forms.Label ( );
        private System.Windows.Forms.Label            Name_Label          = new System.Windows.Forms.Label ( );
        private System.Windows.Forms.Label            Size_Label          = new System.Windows.Forms.Label ( );
        private System.Windows.Forms.ProgressBar      ProgressBar         = new System.Windows.Forms.ProgressBar ( );
        private System.Windows.Forms.Label            UploadRate_Label    = new System.Windows.Forms.Label ( );
        private System.Windows.Forms.Label            RemainingTime_Label = new System.Windows.Forms.Label ( );
        private System.Windows.Forms.TableLayoutPanel TablePanel          = new System.Windows.Forms.TableLayoutPanel ( );

        private System.DateTime                       LastUpdate          = System.DateTime.Now;

        delegate void PositionCallback           ( int position );
        delegate void NameCallback               ( String name );
        delegate void SizeCallback               ( String size );
        delegate void ProgressPercentageCallback ( int percentage );
        delegate void UploadRateCallback         ( double upload_rate );
        delegate void RemainingTimeCallback      ( double time );
        delegate void TransferredBytesCallback   ( long transferred_bytes );
        delegate void VisibleCallback            ( bool visible );

        public UploadTable ( ) : this ( new File ( ) )
        {
        }

        public UploadTable ( File file )
        {
            TabShown++;

            this.TablePanel.SuspendLayout ( );

            this.TablePanel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TablePanel.Name = "TableLayoutPanel";
            this.TablePanel.RowCount = 1;
            this.TablePanel.Location = new System.Drawing.Point( 12, 12 + ( 20 * TabShown ) );
            this.TablePanel.RowStyles.Add ( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Absolute, 28F ) );
            this.TablePanel.Size = new System.Drawing.Size ( 560, 20 );
            this.TablePanel.Visible = true;
            this.TablePanel.TabIndex = TabIndex ++;

            this.Position_Label.AutoSize = true;
            this.Position_Label.Location = new System.Drawing.Point ( 5, 2 );
            this.Position_Label.Name = "Position";
            this.Position_Label.Size = new System.Drawing.Size ( 16, 13 );
            this.Position_Label.TabIndex = TabIndex ++;
            this.Position_Label.Text = TabShown.ToString ( );

            this.Name_Label.AutoSize = true;
            this.Name_Label.Location = new System.Drawing.Point ( 29, 2 );
            this.Name_Label.Name = "Name";
            this.Name_Label.Size = new System.Drawing.Size ( 35, 13 );
            this.Name_Label.TabIndex = TabIndex ++;
            this.Name_Label.Text = file.TimeStamp;

            this.Size_Label.AutoSize = true;
            this.Size_Label.Location = new System.Drawing.Point ( 187, 2 );
            this.Size_Label.Name = "Size";
            this.Size_Label.Size = new System.Drawing.Size ( 52, 13 );
            this.Size_Label.TabIndex = TabIndex ++;
            this.Size_Label.Text = file.Size ( );

            this.ProgressBar.Anchor = ( ( System.Windows.Forms.AnchorStyles ) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left ) | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.ProgressBar.Location = new System.Drawing.Point ( 279, 5 );
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size ( 84, 10 );
            this.ProgressBar.Step = 1;
            this.ProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.ProgressBar.TabIndex = TabIndex ++;
            this.ProgressBar.Value = 0;

            this.UploadRate_Label.AutoSize = true;
            this.UploadRate_Label.Location = new System.Drawing.Point ( 371, 2 );
            this.UploadRate_Label.Name = "UploadRate";
            this.UploadRate_Label.Size = new System.Drawing.Size ( 41, 13 );
            this.UploadRate_Label.TabIndex = TabIndex ++;
            this.UploadRate_Label.Text = String.Empty;

            this.RemainingTime_Label.AutoSize = true;
            this.RemainingTime_Label.Location = new System.Drawing.Point ( 463, 2 );
            this.RemainingTime_Label.Name = "RemainingTime";
            this.RemainingTime_Label.Size = new System.Drawing.Size ( 86, 13 );
            this.RemainingTime_Label.TabIndex = TabIndex ++;
            this.RemainingTime_Label.Text = String.Empty;

            this.TablePanel.Anchor = ( ( System.Windows.Forms.AnchorStyles ) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left ) | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.TablePanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.TablePanel.ColumnCount = 6;
            this.TablePanel.ColumnStyles.Add ( new System.Windows.Forms.ColumnStyle ( System.Windows.Forms.SizeType.Absolute, 22F ) );
            this.TablePanel.ColumnStyles.Add ( new System.Windows.Forms.ColumnStyle ( System.Windows.Forms.SizeType.Absolute, 156F ) );
            this.TablePanel.ColumnStyles.Add ( new System.Windows.Forms.ColumnStyle ( System.Windows.Forms.SizeType.Absolute, 90F ) );
            this.TablePanel.ColumnStyles.Add ( new System.Windows.Forms.ColumnStyle ( System.Windows.Forms.SizeType.Absolute, 90F ) );
            this.TablePanel.ColumnStyles.Add ( new System.Windows.Forms.ColumnStyle ( System.Windows.Forms.SizeType.Absolute, 90F ) );
            this.TablePanel.ColumnStyles.Add ( new System.Windows.Forms.ColumnStyle ( ) );

            this.TablePanel.Controls.Add ( this.Position_Label, 0, 0 );
            this.TablePanel.Controls.Add ( this.Name_Label, 1, 0 );
            this.TablePanel.Controls.Add ( this.Size_Label, 2, 0 );
            this.TablePanel.Controls.Add ( this.ProgressBar, 3, 0 );
            this.TablePanel.Controls.Add ( this.UploadRate_Label, 4, 0 );
            this.TablePanel.Controls.Add ( this.RemainingTime_Label, 5, 0 );

            this.TablePanel.ResumeLayout ( false );
        }

        public void Dispose ( )
        {
            TabIndex -= 7;
            TabShown --;
        }

        public void SetPosition ( int position )
        {
            if ( this.Position_Label.InvokeRequired )
            {
                PositionCallback d = new PositionCallback ( SetPosition );
                this.Position_Label.Invoke ( d, new object [ ] { position } );
            }

            else
            {
                this.Position_Label.Text = position.ToString ( );
            }
        }

        public String GetPosition ( )
        {
            return this.Position_Label.Text;
        }

        public void SetName ( String name )
        {
            if ( this.Name_Label.InvokeRequired )
            {
                NameCallback d = new NameCallback ( SetName );
                this.Name_Label.Invoke ( d, new object [ ] { name } );
            }

            else
            {
                this.Name_Label.Text = name;
            }
        }

        public String GetName ( )
        {
            return this.Name_Label.Text;
        }

        public void SetSize ( String size )
        {
            if ( this.Size_Label.InvokeRequired )
            {
                SizeCallback d = new SizeCallback ( SetSize );
                this.Size_Label.Invoke ( d, new object [ ] { size } );
            }

            else
            {
                this.Size_Label.Text = size;
            }
        }

        public String GetSize ( )
        {
            return this.Size_Label.Text;
        }

        public void SetProgressPercentage ( int percentage )
        {
            if ( this.ProgressBar.InvokeRequired )
            {
                ProgressPercentageCallback d = new ProgressPercentageCallback ( SetProgressPercentage );
                this.ProgressBar.Invoke ( d, new object [ ] { percentage } );
            }

            else
            {
                this.ProgressBar.Value = percentage;
            }
        }

        public int GetProgressPercentage ( )
        {
            return this.ProgressBar.Value;
        }

        public void SetUploadRate ( double upload_rate )
        {
            if ( this.UploadRate_Label.InvokeRequired )
            {
                UploadRateCallback d = new UploadRateCallback ( SetUploadRate );
                this.UploadRate_Label.Invoke ( d, new object [ ] { upload_rate } );
            }

            else
            {
                if ( 0 < upload_rate )
                {
                    this.UploadRate_Label.Text = Utilities.FormatSize ( upload_rate ) + "/s";
                }

                else
                {
                    this.UploadRate_Label.Text = String.Empty;
                }
            }
        }

        public String GetUploadRate ( )
        {
            return this.UploadRate_Label.Text;
        }

        public void SetRemainingTime ( double time )
        {
            if ( this.UploadRate_Label.InvokeRequired )
            {
                RemainingTimeCallback d = new RemainingTimeCallback ( SetRemainingTime );
                this.RemainingTime_Label.Invoke ( d, new object [ ] { time } );
            }

            else
            {
                if ( 0 < time )
                {
                    this.RemainingTime_Label.Text = Utilities.FormatTime ( time );
                }

                else
                {
                    this.RemainingTime_Label.Text = String.Empty;
                }
            }
        }

        public String GetRemainingTime ( )
        {
            return this.RemainingTime_Label.Text;
        }

        public void SetTransferredBytes ( long transferred_bytes )
        {
            if ( this.UploadRate_Label.InvokeRequired )
            {
                TransferredBytesCallback d = new TransferredBytesCallback ( SetTransferredBytes );
                this.RemainingTime_Label.Invoke ( d, new object [ ] { transferred_bytes } );
            }

            else
            {
                this.TransferredBytes = transferred_bytes;
            }
        }

        public long GetTransferredBytes ( )
        {
            return this.TransferredBytes;
        }

        public void SetVisible ( bool visible )
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            try
            {
                if ( this.TablePanel.InvokeRequired )
                {
                    VisibleCallback d = new VisibleCallback ( SetVisible );
                    this.TablePanel.Invoke ( d, new object [ ] { visible } );
                }

                else
                {
                    this.TablePanel.Visible = visible;

                    if ( visible )
                    {
                        TabShown ++;
                    }

                    else
                    {
                        TabShown --;
                    }

                    this.TablePanel.Invalidate ( );
                }
            }

            catch ( Exception e )
            {
                System.Windows.Forms.MessageBox.Show ( e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
                System.Threading.Thread.Sleep ( 10 );
            }
        }

        public TableLayoutPanel TableLayoutPanel
        {
            get { return this.TablePanel; }
        }

        public TableLayoutColumnStyleCollection ColumnStyles
        { 
            get { return this.TablePanel.ColumnStyles; }
        }

        public System.DateTime Time
        {
            set { this.LastUpdate = value; }
            get { return this.LastUpdate; }
        }

        public static int TablesShown ( )
        {
            return TabShown;
        }

        public void RefreshPosition ( int position )
        {
            this.Position_Label.Text = position.ToString ( );

            this.TablePanel.Location = new System.Drawing.Point ( 12, 12 + ( 20 * position ) );

            this.TablePanel.Invalidate ( );
        }
    }
}
