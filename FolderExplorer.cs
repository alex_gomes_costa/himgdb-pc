﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AWS_GUI
{
    class FolderExplorer
    {
        // Create an instance of the folder browser dialog box.
        FolderBrowserDialog FolderBrowserDialog = new FolderBrowserDialog ( );
        
        public FolderExplorer ( ) : this ( Environment.SpecialFolder.Desktop )
        {
        }

        public FolderExplorer ( Environment.SpecialFolder RootFolder )
        {
            FolderBrowserDialog.RootFolder = RootFolder;
        }

        public String GetFolderPath ( String Description = "Selecione a pasta a ser monitorada" )
        {
            FolderBrowserDialog.Description = Description;

            // Process input if the user clicked OK.
            if ( DialogResult.OK == FolderBrowserDialog.ShowDialog ( ) )
            {
                return FolderBrowserDialog.SelectedPath;
            }

            return "";
        }
    }
}
