﻿namespace AWS_GUI
{
    partial class Debug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Debug));
            this.Debug_TextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Debug_TextBox
            // 
            this.Debug_TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Debug_TextBox.Location = new System.Drawing.Point(0, 0);
            this.Debug_TextBox.Multiline = true;
            this.Debug_TextBox.Name = "Debug_TextBox";
            this.Debug_TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Debug_TextBox.Size = new System.Drawing.Size(342, 205);
            this.Debug_TextBox.TabIndex = 0;
            // 
            // Debug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 205);
            this.Controls.Add(this.Debug_TextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Debug";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Debug";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Debug_TextBox;
    }
}