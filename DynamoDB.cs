﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Amazon.Runtime;

namespace AWS_GUI
{
    class DynamoDB
    {
        private static AmazonDynamoDBClient client = new AmazonDynamoDBClient ( );

        private static Debug Debug = new Debug ( "DynamoDB", true );

        public DynamoDB ( )
        {
        }

        public static void DynamoDB_Run ( String TableName )
        {
            CreateTable ( TableName );
            GetTableInformation ( TableName );

            Debug.AppendText ( "Done" );
        }

        public static void CreateTable ( String TableName )
        {
            try
            {
                Debug.AppendText ( "=======================================" );
                Debug.AppendText ( "Creating table..." );

                var request = new CreateTableRequest
                {
                    AttributeDefinitions = new List < AttributeDefinition > ( )
                    {
                        new AttributeDefinition
                        {
                            AttributeName = "Id",
                            AttributeType = "N"
                        },

                        new AttributeDefinition
                        {
                            AttributeName = "ReplyDateTime",
                            AttributeType = "N"
                        }
                    },

                    KeySchema = new List < KeySchemaElement >
                    {
                        new KeySchemaElement
                        {
                            AttributeName = "Id",
                            KeyType = "HASH" //Partition key
                        },

                        new KeySchemaElement
                        {
                            AttributeName = "ReplyDateTime",
                            KeyType = "RANGE" //Sort key
                        }
                    },

                    ProvisionedThroughput = new ProvisionedThroughput
                    {
                        ReadCapacityUnits = 3,
                        WriteCapacityUnits = 3
                    },

                    TableName = TableName
                };

                var response = client.CreateTable ( request );

                var tableDescription = response.TableDescription;

                Debug.AppendText ( String.Format ( "{1}: {0} \t ReadsPerSec: {2} \t WritesPerSec: {3}",
                          tableDescription.TableStatus,
                          tableDescription.TableName,
                          tableDescription.ProvisionedThroughput.ReadCapacityUnits,
                          tableDescription.ProvisionedThroughput.WriteCapacityUnits ) );

                string status = tableDescription.TableStatus;

                Debug.AppendText ( TableName + " - " + status );

                WaitUntilTableReady ( TableName );
            }

            catch ( AmazonDynamoDBException e )
            {
                AmazonDynamoDB_ExceptionHandler ( e );
            }

            catch ( AmazonServiceException e )
            {
                AmazonService_ExceptionHandler ( e );
            }

            catch ( Exception e )
            {
                ExceptionHandler ( e );
            }
        }

        public static void ListTables ( )
        {
            try
            {
                Debug.AppendText ( "=======================================" );
                Debug.AppendText ( "Listing tables..." );

                string lastTableNameEvaluated = null;

                do
                {
                    var request = new ListTablesRequest
                    {
                        Limit = 10,
                        ExclusiveStartTableName = lastTableNameEvaluated
                    };

                    var response = client.ListTables( request );

                    foreach ( string name in response.TableNames )
                    {
                        Debug.AppendText ( name );
                    }

                    lastTableNameEvaluated = response.LastEvaluatedTableName;

                } while ( lastTableNameEvaluated != null );
            }

            catch ( AmazonDynamoDBException e )
            {
                AmazonDynamoDB_ExceptionHandler ( e );
            }

            catch ( AmazonServiceException e )
            {
                AmazonService_ExceptionHandler ( e );
            }

            catch ( Exception e )
            {
                ExceptionHandler ( e );
            }
        }

        public static void GetTableInformation ( String TableName )
        {
            try
            {
                Debug.AppendText ( "=======================================" );
                Debug.AppendText ( "Retrieving table information..." );

                var request = new DescribeTableRequest
                {
                    TableName = TableName
                };

                var response = client.DescribeTable( request );

                TableDescription description = response.Table;

                Debug.AppendText ( String.Format ( "Name: {0}", description.TableName ) );
                Debug.AppendText ( String.Format ( "# of items: {0}", description.ItemCount ) );
                Debug.AppendText ( String.Format ( "Provision Throughput (reads/sec): {0}",
                          description.ProvisionedThroughput.ReadCapacityUnits ) );
                Debug.AppendText ( String.Format ( "Provision Throughput (writes/sec): {0}",
                          description.ProvisionedThroughput.WriteCapacityUnits ) );
            }

            catch ( AmazonDynamoDBException e )
            {
                AmazonDynamoDB_ExceptionHandler ( e );
            }

            catch ( AmazonServiceException e )
            {
                AmazonService_ExceptionHandler ( e );
            }

            catch ( Exception e )
            {
                ExceptionHandler ( e );
            }
        }

        public static void DeleteTable ( String TableName )
        {
            try
            {
                Debug.AppendText ( "=======================================" );
                Debug.AppendText ( "Deleting table..." );

                var request = new DeleteTableRequest
                {
                    TableName = TableName
                };

                var response = client.DeleteTable ( request );

                Debug.AppendText ( "Table is being deleted..." );
            }

            catch ( AmazonDynamoDBException e )
            {
                AmazonDynamoDB_ExceptionHandler ( e );
            }

            catch ( AmazonServiceException e )
            {
                AmazonService_ExceptionHandler ( e );
            }

            catch ( Exception e )
            {
                ExceptionHandler ( e );
            }
        }

        private static void WaitUntilTableReady ( String TableName )
        {
            try
            {
                string status = null;

                // Let us wait until table is created. Call DescribeTable.
                do
                {
                    System.Threading.Thread.Sleep ( 2000 ); // Wait 2 seconds.

                    var res = client.DescribeTable ( new DescribeTableRequest
                    {
                        TableName = TableName
                    } );

                    Debug.AppendText ( String.Format ( "Table name: {0}, status: {1}",
                                res.Table.TableName,
                                res.Table.TableStatus ) );
                    status = res.Table.TableStatus;

                } while ( "ACTIVE" != status );
            }

            catch ( AmazonDynamoDBException e )
            {
                AmazonDynamoDB_ExceptionHandler ( e );
            }

            catch ( AmazonServiceException e )
            {
                AmazonService_ExceptionHandler ( e );
            }

            catch ( Exception e )
            {
                ExceptionHandler ( e );
            }
        }

        private static void AmazonDynamoDB_ExceptionHandler ( AmazonDynamoDBException AmazonDynamoDBException )
        {
            Debug.AppendText ( String.Format ( "An error occurred with the message '{0}'. ", AmazonDynamoDBException.Message ) );
        }

        private static void AmazonService_ExceptionHandler ( AmazonServiceException AmazonServiceException )
        {
            Debug.AppendText ( String.Format ( "An error occurred with the message '{0}'. ", AmazonServiceException.Message ) );
        }

        private static void ExceptionHandler ( Exception Exception )
        {
            Debug.AppendText ( String.Format ( "An error occurred with the message '{0}'. ", Exception.Message ) );
        }
    }
}
