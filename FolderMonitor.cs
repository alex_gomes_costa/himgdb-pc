﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace AWS_GUI
{
    class FolderMonitor
    {
        static FileSystemWatcher FileSystemWatcher    = new FileSystemWatcher ( );
        static List < Thread >   FileUploader_Threads = new List < Thread > ( );
        static List < File >     PendingActions       = new List < File > ( );
        static List < File >     Uploads              = new List < File > ( );
        static Mutex             Mutex_Pending        = new Mutex ( );
        static Mutex             Mutex_Upload         = new Mutex ( );
        static Mutex             Mutex_Thread         = new Mutex ( );
        static S3                S3                   = new S3 ( );
        static Random            Random               = new Random ( );

        static bool              Halt                 = true;

        Debug Debug = new Debug ( "FolderMonitor", false );

        public FolderMonitor ( ) : this ( "folder.txt" )
        {
        }

        ~FolderMonitor ( )
        {
            foreach ( Thread thread in FileUploader_Threads )
            {
                if ( thread.IsAlive )
                {
                    // Request that thread be stopped
                    thread.Abort( );

                    // Wait until thread finishes. Join also has overloads
                    // that take a millisecond interval or a TimeSpan object.
                    thread.Join( );
                }
            }
        }

        public FolderMonitor ( String ConfigurationFile )
        {
            try
            {
                String Path = RetrieveFolder ( ConfigurationFile );

                System.IO.Directory.CreateDirectory ( Path );

                // Associate event handlers with the events
                FileSystemWatcher.Created += FileCreated;
                FileSystemWatcher.Changed += FileChanged;

                // Tell the monitor where to look
                FileSystemWatcher.Path = Path;

                // Allows events to fire.
                FileSystemWatcher.EnableRaisingEvents = true;
            }

            catch ( Exception ex )
            {
                System.Windows.Forms.MessageBox.Show ( ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK );

                Environment.Exit ( 3 );
            }
        }

        public static void StartFolderMonitor ( )
        {
            const int UPLOADS_LIMIT = 5;

            while ( true )
            {
                if ( !Halt )
                {
                    // Wait until it is safe to enter.
                    Mutex_Upload.WaitOne  ( );
                    Mutex_Pending.WaitOne ( );

                    // Limit to 5 parallel uploads
                    while ( UPLOADS_LIMIT > Uploads.Count )
                    {
                        if ( 0 < PendingActions.Count )
                        {
                            CreateThread ( PendingActions [ 0 ] );
                        
                            PendingActions.RemoveAt ( 0 );
                        }
                        
                        else
                        {
                            break;
                        }
                    };

                    // Release the Mutexes.
                    Mutex_Pending.ReleaseMutex( );
                    Mutex_Upload.ReleaseMutex ( );

                    DeleteThreads ( );
                }

                // Sleep for a while - to let the system refresh itself
                Thread.Sleep ( 1000 );
            };
        }

        public bool HaltService
        {
            set { Halt = value; }
            get { return Halt; }
        }

        private void FileChanged ( object sender, FileSystemEventArgs e )
        {
            if ( AddPendingActions ( e.FullPath ) )
            {
                Debug.AppendText ( String.Format ( "A file has been changed - {0}", e.Name ) );
            }
        }

        private void FileCreated ( object sender, FileSystemEventArgs e )
        {
            if ( AddPendingActions ( e.FullPath ) )
            {
                Debug.AppendText ( String.Format ( "A new file has been created - {0}", e.Name ) );
            }
        }

        private bool AddPendingActions ( String FullPath )
        {
            bool new_file = true;

            // Wait until it is safe to enter.
            Mutex_Pending.WaitOne ( );

            foreach ( File file in PendingActions )
            {
                if ( FullPath == file.FullPath )
                {
                    new_file = false;

                    break;
                }
            }

            if ( new_file )
            {
                File NewFile = new File ( FullPath );

                PendingActions.Add ( NewFile );
            }

            // Release the Mutex.
            Mutex_Pending.ReleaseMutex ( );

            return new_file;
        }

        private static void RemoveUpload ( String FullPath )
        {
            // Wait until it is safe to enter.
            Mutex_Upload.WaitOne ( );

            foreach ( File file in Uploads )
            {
                if ( FullPath == file.FullPath )
                {
                    Uploads.Remove ( file );

                    break;
                }
            }

            // Release the Mutex.
            Mutex_Upload.ReleaseMutex ( );
        }

        private String RetrieveFolder ( string ConfigurationFile )
        {
            String Path = String.Empty;

            try
            {
                if ( System.IO.File.Exists ( ConfigurationFile ) )
                {
                    StreamReader StreamReader = new StreamReader ( ConfigurationFile );

                    Path = StreamReader.ReadLine ( );

                    StreamReader.Close ( );
                }

                if ( string.IsNullOrEmpty ( Path ) )
                {
                    FolderExplorer FolderExplorer = new FolderExplorer ( );

                    Path = FolderExplorer.GetFolderPath ( );

                    StreamWriter StreamWriter = new StreamWriter ( ConfigurationFile );

                    StreamWriter.WriteLine ( Path );

                    StreamWriter.Close ( );
                }
            }

            catch ( Exception ex )
            {
                System.Windows.Forms.MessageBox.Show ( ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK );
            }

            return Path;
        }

        private static void FileUploader ( File file )
        {
            bool Uploading = true;

            while ( Uploading )
            {
                try
                {
                    // Ensures that the file is ready to use
                    FileStream FileStream = new FileStream ( file.FullPath, FileMode.Open );
                    FileStream.Close ( );

                    // Send this file to AWS
                    if ( S3.UploadingAnObject ( file ) )
                    {
                        RemoveUpload ( file.FullPath );

                        Uploading = false;
                    }
                }

                catch { }
            }
        }

        private static void CreateThread ( File file )
        {
            Uploads.Add ( file );

            // Creates a new thread
            Thread NewThread = new Thread ( ( ) => FolderMonitor.FileUploader ( file ) );

            // Start the thread
            NewThread.Start ( );

            // Spin for a while waiting for the started thread to become alive
            while ( !NewThread.IsAlive );

            // Wait until it is safe to enter.
            Mutex_Thread.WaitOne ( );

            // Inserts the thread to the list
            FileUploader_Threads.Add ( NewThread );

            // Release the Mutex.
            Mutex_Thread.ReleaseMutex ( );

            // Put the Main thread to sleep from 1~5 millisecond to allow
            // FolderMonitor_Thread to do some work:
            Thread.Sleep ( Random.Next ( 1, 5 ) );
        }

        private static void DeleteThreads ( )
        {
            // Wait until it is safe to enter.
            Mutex_Thread.WaitOne ( );

            List < int > Indexes = new List < int > ( );

            for ( int index = 0; index < FileUploader_Threads.Count; index ++ )
            {
                if ( !FileUploader_Threads [ index ].IsAlive )
                {
                    Indexes.Add ( index );
                }
            }

            Indexes.Reverse ( );

            foreach ( int index in Indexes )
            {
                FileUploader_Threads.RemoveAt ( index );
            }

            // Release the Mutex.
            Mutex_Thread.ReleaseMutex ( );
        }
    }
}
