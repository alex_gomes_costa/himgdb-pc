﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace AWS_GUI
{
    public class File
    {
        private String Fullpath;
        private String Hash_MD5;
        private String Timestamp;

        public File ( ) : this ( String.Empty )
        {
        }

        public File ( String FullPath )
        {
            Fullpath  = FullPath;

            Hash_MD5  = String.Empty;
            Timestamp = String.Empty;
        }

        public String FullPath
        {
            get { return Fullpath; }
        }

        public String HashMD5
        {
            get { return GenerateMD5 ( FullPath ); }
            set { Hash_MD5 = value; }
        }

        public String TimeStamp
        {
            get { return GenerateTimestamp ( FullPath ); }
            set { Timestamp = value; }
        }

        public String Size ( )
        {
            long size = 0;

            if ( System.IO.File.Exists ( FullPath ) )
            {
                size = new System.IO.FileInfo ( FullPath ).Length;
            }

            return Utilities.FormatSize ( size );
        }

        private String GenerateMD5 ( String FullPath )
        {
            if ( String.IsNullOrEmpty ( Hash_MD5 ) )
            {
                using ( FileStream FileStream = new FileStream ( FullPath, FileMode.Open ) )
                {
                    Hash_MD5 = Convert.ToBase64String ( new MD5CryptoServiceProvider ( ).ComputeHash ( FileStream ) );
                }
            }

            return Hash_MD5;
        }

        private String GenerateTimestamp ( String FullPath )
        {
            if ( String.IsNullOrEmpty ( Timestamp ) )
            {
                try
                {
                    int Length = FullPath.Length;
                    int Dot    = FullPath.LastIndexOf ( '.' );

                    String Extension = FullPath.Substring ( Dot, Length - Dot );

                    Timestamp = ( System.DateTime.Now.ToString ( "yyyy-MM-dd_HH-mm-ss.ffffff" ) + Extension );
                }

                catch
                {
                    Timestamp = ( System.DateTime.Now.ToString ( "yyyy-MM-dd_HH-mm-ss.ffffff" ) );
                }
            }

            return Timestamp;
        }
    }
}
