﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AWS_GUI
{
    public partial class GUI : Form
    {
        //DynamoDB DynamoDB = new DynamoDB ( );

        public GUI ( )
        {
            InitializeComponent ( );

            // Load profile configuration
            LoadProfile ( );

            // Start the thread
            FolderMonitor_Thread.Start ( );

            // Spin for a while waiting for the started thread to become alive
            while ( !FolderMonitor_Thread.IsAlive );

            // Put the Main thread to sleep for 1 millisecond to allow
            // FolderMonitor_Thread to do some work:
            Thread.Sleep ( 1 );

            /*
            DynamoDB.DynamoDB_Run ( "Teste1" );
            DynamoDB.DynamoDB_Run ( "Teste2" );
            DynamoDB.DynamoDB_Run ( "Teste3" );
            DynamoDB.DynamoDB_Run ( "Teste4" );
            DynamoDB.DynamoDB_Run ( "Teste5" );
            DynamoDB.DynamoDB_Run ( "Teste6" );
            DynamoDB.DynamoDB_Run ( "Teste7" );
            DynamoDB.DynamoDB_Run ( "Teste8" );

            DynamoDB.ListTables ( );

            DynamoDB.DeleteTable ( "Teste1" );
            DynamoDB.DeleteTable ( "Teste2" );
            DynamoDB.DeleteTable ( "Teste3" );
            DynamoDB.DeleteTable ( "Teste4" );
            DynamoDB.DeleteTable ( "Teste5" );
            DynamoDB.DeleteTable ( "Teste6" );
            DynamoDB.DeleteTable ( "Teste7" );
            DynamoDB.DeleteTable ( "Teste8" );
            */
        }

        private void LoadProfile ( )
        {
            try
            {
                using ( StreamReader StreamReader = new StreamReader ( "login.txt" ) )
                {
                    this.RememberMe_CheckBox.Checked = true;

                    this.UserName_TextBox.Text = StreamReader.ReadLine ( );
                    this.Password_TextBox.Text = StreamReader.ReadLine ( );

                    if ( "autologin" == StreamReader.ReadLine ( ) )
                    {
                        this.AutoLogin_CheckBox.Checked = true;
                    }

                    else
                    {
                        this.AutoLogin_CheckBox.Checked = false;
                    }
                }

                if ( this.AutoLogin_CheckBox.Checked )
                {
                    Login ( );
                }
            }

            catch
            {
                this.UserName_TextBox.Text = String.Empty;
                this.Password_TextBox.Text = String.Empty;

                this.RememberMe_CheckBox.Checked = false;
                this.AutoLogin_CheckBox.Checked  = false;
            }
        }

        private void SaveProfile ( )
        {
            try
            {
                if ( this.RememberMe_CheckBox.Checked )
                {
                    using ( StreamWriter StreamWriter = new StreamWriter ( "login.txt", false ) )
                    {
                        StreamWriter.WriteLine ( this.UserName_TextBox.Text );
                        StreamWriter.WriteLine ( this.Password_TextBox.Text );

                        if ( this.AutoLogin_CheckBox.Checked )
                        {
                            StreamWriter.WriteLine ( "autologin" );
                        }
                    }
                }

                else
                {
                    if ( System.IO.File.Exists ( "login.txt" ) )
                    {
                        System.IO.File.Delete ( "login.txt" );
                    }
                }
            }

            catch ( Exception ex )
            {
                System.Windows.Forms.MessageBox.Show( ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK );
            }
        }

        private void Login ( )
        {
            try
            {
                if ( "Login" == this.Login_Button.Text )
                {
                    using ( StreamReader StreamReader = new StreamReader ( "user.txt" ) )
                    {
                        if ( ( this.UserName_TextBox.Text == StreamReader.ReadLine ( ) ) &&
                             ( this.Password_TextBox.Text == StreamReader.ReadLine ( ) ) )
                        {
                            SaveProfile ( );

                            this.UserName_TextBox.Enabled    = false;
                            this.Password_TextBox.Enabled    = false;
                            this.RememberMe_CheckBox.Enabled = false;
                            this.AutoLogin_CheckBox.Enabled  = false;

                            this.Login_Button.Text = "Logout";

                            this.WindowState = FormWindowState.Minimized;

                            this.NotifyIcon.ShowBalloonTip ( 1500 );
                        }

                        else
                        {
                            System.Windows.Forms.MessageBox.Show ( "Usuário ou senha inválidos", "Error", System.Windows.Forms.MessageBoxButtons.OK );
                        }
                    }

                    FolderMonitor.HaltService = false;
                }

                else if ( "Logout" == this.Login_Button.Text )
                {
                    this.UserName_TextBox.Enabled    = true;
                    this.Password_TextBox.Enabled    = true;
                    this.RememberMe_CheckBox.Enabled = true;
                    this.AutoLogin_CheckBox.Enabled  = true;

                    this.Login_Button.Text = "Login";

                    FolderMonitor.HaltService = true;
                }
            }

            catch ( Exception ex )
            {
                System.Windows.Forms.MessageBox.Show ( ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK );
            }
        }

        private void Login_Button_Click ( object sender, EventArgs e )
        {
            Login ( );
        }

        private void Notify_MouseDoubleClick ( object sender, MouseEventArgs e )
        {
            if ( FormWindowState.Normal == this.WindowState )
            {
                this.WindowState = FormWindowState.Minimized;

                this.NotifyIcon.ShowBalloonTip ( 1500 );
            }

            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }
    }
}
