﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AWS_GUI
{
    public partial class Debug : Form
    {
        delegate void AppendTextCallback ( String text );

        public Debug ( ) : this ( String.Empty, false )
        {
        }

        public Debug ( String Title, bool show )
        {
            InitializeComponent ( );

            if ( !String.IsNullOrEmpty ( Title ) )
            {
                this.Text += " - " + Title;
            }

            if ( show )
            {
                this.Show ( );
            }
        }

        public void AppendText ( String text )
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            try
            {
                if ( this.Debug_TextBox.InvokeRequired )
                {
                    AppendTextCallback d = new AppendTextCallback ( AppendText );
                    this.Invoke ( d, new object [ ] { text } );
                }

                else
                {
                    if ( this.Visible )
                    {
                        this.Debug_TextBox.AppendText ( text + System.Environment.NewLine );
                    }
                }
            }

            catch ( Exception e )
            {
                System.Windows.Forms.MessageBox.Show ( e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
                System.Threading.Thread.Sleep ( 10 );
            }
        }

        public void ClearText ( )
        {
            this.Debug_TextBox.Clear ( );
        }
    }
}
