﻿using System.Threading;
using System.Threading.Tasks;

namespace AWS_GUI
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        FolderMonitor FolderMonitor = new FolderMonitor ( );

        // Initialize a new thread 
        Thread FolderMonitor_Thread = new Thread ( FolderMonitor.StartFolderMonitor );

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            // Request that FolderMonitor_Thread be stopped
            FolderMonitor_Thread.Abort ( );

            // Wait until FolderMonitor_Thread finishes. Join also has overloads
            // that take a millisecond interval or a TimeSpan object.
            FolderMonitor_Thread.Join ( );

            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GUI));
            this.UserName_TextBox = new System.Windows.Forms.TextBox();
            this.Password_TextBox = new System.Windows.Forms.TextBox();
            this.UserName_Label = new System.Windows.Forms.Label();
            this.Password_Label = new System.Windows.Forms.Label();
            this.Logo_PictureBox = new System.Windows.Forms.PictureBox();
            this.RememberMe_CheckBox = new System.Windows.Forms.CheckBox();
            this.AutoLogin_CheckBox = new System.Windows.Forms.CheckBox();
            this.Login_Button = new System.Windows.Forms.Button();
            this.NotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Logo_PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // UserName_TextBox
            // 
            this.UserName_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName_TextBox.Location = new System.Drawing.Point(12, 163);
            this.UserName_TextBox.Name = "UserName_TextBox";
            this.UserName_TextBox.Size = new System.Drawing.Size(363, 26);
            this.UserName_TextBox.TabIndex = 0;
            // 
            // Password_TextBox
            // 
            this.Password_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Password_TextBox.Location = new System.Drawing.Point(12, 215);
            this.Password_TextBox.Name = "Password_TextBox";
            this.Password_TextBox.PasswordChar = '*';
            this.Password_TextBox.Size = new System.Drawing.Size(363, 26);
            this.Password_TextBox.TabIndex = 1;
            // 
            // UserName_Label
            // 
            this.UserName_Label.AutoSize = true;
            this.UserName_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName_Label.Location = new System.Drawing.Point(12, 140);
            this.UserName_Label.Name = "UserName_Label";
            this.UserName_Label.Size = new System.Drawing.Size(129, 20);
            this.UserName_Label.TabIndex = 2;
            this.UserName_Label.Text = "Nome de usuário";
            // 
            // Password_Label
            // 
            this.Password_Label.AutoSize = true;
            this.Password_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Password_Label.Location = new System.Drawing.Point(12, 192);
            this.Password_Label.Name = "Password_Label";
            this.Password_Label.Size = new System.Drawing.Size(56, 20);
            this.Password_Label.TabIndex = 3;
            this.Password_Label.Text = "Senha";
            // 
            // Logo_PictureBox
            // 
            this.Logo_PictureBox.Image = ((System.Drawing.Image)(resources.GetObject("Logo_PictureBox.Image")));
            this.Logo_PictureBox.InitialImage = null;
            this.Logo_PictureBox.Location = new System.Drawing.Point(12, 12);
            this.Logo_PictureBox.Name = "Logo_PictureBox";
            this.Logo_PictureBox.Size = new System.Drawing.Size(363, 125);
            this.Logo_PictureBox.TabIndex = 4;
            this.Logo_PictureBox.TabStop = false;
            // 
            // RememberMe_CheckBox
            // 
            this.RememberMe_CheckBox.AutoSize = true;
            this.RememberMe_CheckBox.Checked = true;
            this.RememberMe_CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RememberMe_CheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RememberMe_CheckBox.Location = new System.Drawing.Point(12, 247);
            this.RememberMe_CheckBox.Name = "RememberMe_CheckBox";
            this.RememberMe_CheckBox.Size = new System.Drawing.Size(114, 24);
            this.RememberMe_CheckBox.TabIndex = 5;
            this.RememberMe_CheckBox.Text = "Lembrar-me";
            this.RememberMe_CheckBox.UseVisualStyleBackColor = true;
            // 
            // AutoLogin_CheckBox
            // 
            this.AutoLogin_CheckBox.AutoSize = true;
            this.AutoLogin_CheckBox.Checked = true;
            this.AutoLogin_CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AutoLogin_CheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoLogin_CheckBox.Location = new System.Drawing.Point(12, 277);
            this.AutoLogin_CheckBox.Name = "AutoLogin_CheckBox";
            this.AutoLogin_CheckBox.Size = new System.Drawing.Size(150, 24);
            this.AutoLogin_CheckBox.TabIndex = 6;
            this.AutoLogin_CheckBox.Text = "Login automático";
            this.AutoLogin_CheckBox.UseVisualStyleBackColor = true;
            // 
            // Login_Button
            // 
            this.Login_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login_Button.Location = new System.Drawing.Point(285, 257);
            this.Login_Button.Name = "Login_Button";
            this.Login_Button.Size = new System.Drawing.Size(90, 33);
            this.Login_Button.TabIndex = 7;
            this.Login_Button.Text = "Login";
            this.Login_Button.UseVisualStyleBackColor = true;
            this.Login_Button.Click += new System.EventHandler(this.Login_Button_Click);
            // 
            // NotifyIcon
            // 
            this.NotifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.NotifyIcon.BalloonTipText = "O programa está sendo executado em background.";
            this.NotifyIcon.Text = "THC";
            this.NotifyIcon.Visible = true;
            this.NotifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Notify_MouseDoubleClick);
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 311);
            this.Controls.Add(this.Login_Button);
            this.Controls.Add(this.AutoLogin_CheckBox);
            this.Controls.Add(this.RememberMe_CheckBox);
            this.Controls.Add(this.Logo_PictureBox);
            this.Controls.Add(this.Password_Label);
            this.Controls.Add(this.UserName_Label);
            this.Controls.Add(this.Password_TextBox);
            this.Controls.Add(this.UserName_TextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "GUI";
            this.Text = "THC";
            ((System.ComponentModel.ISupportInitialize)(this.Logo_PictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox UserName_TextBox;
        private System.Windows.Forms.TextBox Password_TextBox;
        private System.Windows.Forms.Label UserName_Label;
        private System.Windows.Forms.Label Password_Label;
        private System.Windows.Forms.PictureBox Logo_PictureBox;
        private System.Windows.Forms.CheckBox RememberMe_CheckBox;
        private System.Windows.Forms.CheckBox AutoLogin_CheckBox;
        private System.Windows.Forms.Button Login_Button;
        private System.Windows.Forms.NotifyIcon NotifyIcon;
    }
}

