﻿namespace AWS_GUI
{
    partial class Uploader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.RemainingTime_Label = new System.Windows.Forms.Label();
            this.Upload_Label = new System.Windows.Forms.Label();
            this.Progress_Label = new System.Windows.Forms.Label();
            this.Size_Label = new System.Windows.Forms.Label();
            this.Position_Label = new System.Windows.Forms.Label();
            this.Name_Label = new System.Windows.Forms.Label();
            this.UploadInfos_TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.UploadInfos_TableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // RemainingTime_Label
            // 
            this.RemainingTime_Label.AutoSize = true;
            this.RemainingTime_Label.Location = new System.Drawing.Point(463, 2);
            this.RemainingTime_Label.Name = "RemainingTime_Label";
            this.RemainingTime_Label.Size = new System.Drawing.Size(86, 13);
            this.RemainingTime_Label.TabIndex = 5;
            this.RemainingTime_Label.Text = "Tempo Restante";
            // 
            // Upload_Label
            // 
            this.Upload_Label.AutoSize = true;
            this.Upload_Label.Location = new System.Drawing.Point(371, 2);
            this.Upload_Label.Name = "Upload_Label";
            this.Upload_Label.Size = new System.Drawing.Size(41, 13);
            this.Upload_Label.TabIndex = 4;
            this.Upload_Label.Text = "Upload";
            // 
            // Progress_Label
            // 
            this.Progress_Label.AutoSize = true;
            this.Progress_Label.Location = new System.Drawing.Point(279, 2);
            this.Progress_Label.Name = "Progress_Label";
            this.Progress_Label.Size = new System.Drawing.Size(54, 13);
            this.Progress_Label.TabIndex = 3;
            this.Progress_Label.Text = "Progresso";
            // 
            // Size_Label
            // 
            this.Size_Label.AutoSize = true;
            this.Size_Label.Location = new System.Drawing.Point(187, 2);
            this.Size_Label.Name = "Size_Label";
            this.Size_Label.Size = new System.Drawing.Size(52, 13);
            this.Size_Label.TabIndex = 2;
            this.Size_Label.Text = "Tamanho";
            // 
            // Position_Label
            // 
            this.Position_Label.AutoSize = true;
            this.Position_Label.Location = new System.Drawing.Point(5, 2);
            this.Position_Label.Name = "Position_Label";
            this.Position_Label.Size = new System.Drawing.Size(16, 13);
            this.Position_Label.TabIndex = 0;
            this.Position_Label.Text = "n.";
            // 
            // Name_Label
            // 
            this.Name_Label.AutoSize = true;
            this.Name_Label.Location = new System.Drawing.Point(29, 2);
            this.Name_Label.Name = "Name_Label";
            this.Name_Label.Size = new System.Drawing.Size(35, 13);
            this.Name_Label.TabIndex = 1;
            this.Name_Label.Text = "Nome";
            // 
            // UploadInfos_TableLayoutPanel
            // 
            this.UploadInfos_TableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UploadInfos_TableLayoutPanel.BackColor = System.Drawing.SystemColors.Window;
            this.UploadInfos_TableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.UploadInfos_TableLayoutPanel.ColumnCount = 6;
            this.UploadInfos_TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.UploadInfos_TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 156F));
            this.UploadInfos_TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.UploadInfos_TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.UploadInfos_TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.UploadInfos_TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.UploadInfos_TableLayoutPanel.Controls.Add(this.Name_Label, 1, 0);
            this.UploadInfos_TableLayoutPanel.Controls.Add(this.Position_Label, 0, 0);
            this.UploadInfos_TableLayoutPanel.Controls.Add(this.Progress_Label, 3, 0);
            this.UploadInfos_TableLayoutPanel.Controls.Add(this.Upload_Label, 4, 0);
            this.UploadInfos_TableLayoutPanel.Controls.Add(this.RemainingTime_Label, 5, 0);
            this.UploadInfos_TableLayoutPanel.Controls.Add(this.Size_Label, 2, 0);
            this.UploadInfos_TableLayoutPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.UploadInfos_TableLayoutPanel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.UploadInfos_TableLayoutPanel.Location = new System.Drawing.Point(12, 12);
            this.UploadInfos_TableLayoutPanel.Name = "UploadInfos_TableLayoutPanel";
            this.UploadInfos_TableLayoutPanel.RowCount = 1;
            this.UploadInfos_TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.UploadInfos_TableLayoutPanel.Size = new System.Drawing.Size(560, 20);
            this.UploadInfos_TableLayoutPanel.TabIndex = 0;
            this.UploadInfos_TableLayoutPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Uploader_MouseDown);
            this.UploadInfos_TableLayoutPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Uploader_MouseMove);
            this.UploadInfos_TableLayoutPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Uploader_MouseUp);
            // 
            // Uploader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 144);
            this.Controls.Add(this.UploadInfos_TableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Uploader";
            this.Text = "THC";
            this.UploadInfos_TableLayoutPanel.ResumeLayout(false);
            this.UploadInfos_TableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
        }

        private void UploadInfos_TableLayoutPanel_MouseDown( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            throw new System.NotImplementedException( );
        }

        #endregion
        private System.Windows.Forms.Label RemainingTime_Label;
        private System.Windows.Forms.Label Upload_Label;
        private System.Windows.Forms.Label Progress_Label;
        private System.Windows.Forms.Label Size_Label;
        private System.Windows.Forms.Label Position_Label;
        private System.Windows.Forms.Label Name_Label;
        private System.Windows.Forms.TableLayoutPanel UploadInfos_TableLayoutPanel;
    }
}